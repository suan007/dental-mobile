import React from 'react';
import { Button, Text, View } from 'react-native';
import styled from 'styled-components/native';
import GrayText from '../components/GrayText';
import { CustomButton } from '../components/CustomButton';
import { Feather } from '@expo/vector-icons';

const PatientScreen = ({ navigation }) => {
    return (
        <Container>
            <PatientFullname>Марина Алмазова</PatientFullname>
            <GrayText>+7 952 444 58 99</GrayText>
            <PatientButtons>
                <ClientFormView>
                    <CustomButton>45678963</CustomButton>
                </ClientFormView>
                <CallButtonView>
                    <CustomButton color="#84D269">
                        <Feather name="phone" size={22} color="white" />
                    </CustomButton>
                </CallButtonView>
            </PatientButtons>
        </Container>
    );
}
const ClientFormView = styled.View`
    flex: 1;
`;
const CallButtonView = styled.View`
    margin-left: 10px;
    width: 45px;

`;
const PatientButtons = styled.View`
    display: flex;
    margin-top: 20px;
    flex-direction: row;
`;

const PatientFullname = styled.Text`
    font-weight: 800;
    font-size: 28px;
    line-height: 30px;
    margin-bottom: 5px;
`;
const Container = styled.View`
    padding: 25px;
`;
export default PatientScreen;
