import React from 'react';
import { Text, View, Button, SectionList } from 'react-native';
import styled from 'styled-components/native';
import HomeScreen from './screens/HomeScreen';
import PatientScreen from './screens/PatientScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Home',
            headerTintColor: '#2A86FF'
          }}
        />
        <Stack.Screen
          name="Patient"
          component={PatientScreen}
          options={{
            title: 'Карта пациента',
            headerTintColor: '#2A86FF',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

