import React from 'react';
import styled from 'styled-components/native';
import { Text, View, Button } from 'react-native';
import GrayText from './GrayText';

export const Group = ({ user, active, time, navigate }) => {
    return (
        <ContainerGroup>
            <GroupItem onPress={() => navigate('Patient')} >
                <Avatar source={{ uri: user.ava }} />
                <View style={{ flex: 1 }}>
                    <FullName>{user.fullName}</FullName>
                    <GrayText>{user.diagnosis}</GrayText>
                </View>
                <GroupDate active={active}>{time}</GroupDate>
            </GroupItem>
        </ContainerGroup>
    );
};
Group.defaultProps = {
    active: false,
    time: '00:00',
    user:  {
        fullName: 'undefined Name',
        diagnosis: 'udefined diagnosis'
    }
}

const GroupDate = styled.Text`
  background: ${props => props.active ? '#2A86FF' : '#e9f5ff'};
  color: ${props => props.active ? '#fff' : '#4294ff'};
  border-radius: 18px;
  font-weight: 600;
  font-size: 14px;
  width: 70px;
  height: 32px;
  text-align: center;
  line-height: 30px;
`;

const FullName = styled.Text`
 font-weight: 600;
 font-size: 16px;
 color: #000000
`;
const Avatar = styled.Image`
  width: 40px;
  height:40px;
  border-radius: 50px;
  margin-right: 20px;
`;
const GroupItem = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  padding: 20px;
  border-bottom-width: 1px;
  border-bottom-color: #f3f3f3;
`;
const ContainerGroup = styled.View`
  padding: 0 20px;
  margin-bottom: 25px;
`;
