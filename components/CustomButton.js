import React from "react";
import styled from "styled-components/native";
import { Text } from "react-native";

export const CustomButton = ({ children, color }) => {
    return (
        <Button color={color}>
            <BottonText >
                {children}
            </BottonText>
        </Button>
    )
}
CustomButton.defaultProps = {
    color: '#2a86ff'
}

const BottonText = styled.Text`
    color: #ffffff;
    font-weight: 500;
    font-size: 16px;
`;


const Button = styled.TouchableOpacity`
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 30px;
    background: ${props => props.color};
    height: 45px;
`;