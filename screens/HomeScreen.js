import React from 'react';
import { Text, View, Button, SectionList } from 'react-native';
import styled from 'styled-components/native';
import { Ionicons } from '@expo/vector-icons';
import { Group } from './../components/Group';
import { GroupTitle } from './../components/GroupTitle';



const DATA = [
    {
        title: '10 октября',
        data: [
            {
                time: '15:30',
                active: true,
                user: {
                    fullName: 'Абзал Суан',
                    diagnosis: 'удаление налета',
                    ava: 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png',
                }
            },
            {
                time: '17:30',
                user: {
                    fullName: 'Мартинас Оляка',
                    diagnosis: 'удаление зуба',
                    ava: 'https://static.thenounproject.com/png/363639-200.png',
                }
            }
        ]
    },
    {
        title: '10 октября',
        data: [
            {
                time: '15:30',
                active: true,
                user: {
                    fullName: 'Абзал Суан',
                    diagnosis: 'удаление налета',
                    ava: 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png',
                }
            },
            {
                time: '17:30',
                user: {
                    fullName: 'Мартинас Оляка',
                    diagnosis: 'удаление зуба',
                    ava: 'https://static.thenounproject.com/png/363639-200.png',
                }
            }
        ]
    },
    {
        title: '10 октября',
        data: [
            {
                time: '15:30',
                active: true,
                user: {
                    fullName: 'Абзал Суан',
                    diagnosis: 'удаление налета',
                    ava: 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png',
                }
            },
            {
                time: '17:30',
                user: {
                    fullName: 'Мартинас Оляка',
                    diagnosis: 'удаление зуба',
                    ava: 'https://static.thenounproject.com/png/363639-200.png',
                }
            }
        ]
    },
    {
        title: '10 октября',
        data: [
            {
                time: '15:30',
                active: true,
                user: {
                    fullName: 'Абзал Суан',
                    diagnosis: 'удаление налета',
                    ava: 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png',
                }
            },
            {
                time: '17:30',
                user: {
                    fullName: 'Мартинас Оляка',
                    diagnosis: 'удаление зуба',
                    ava: 'https://static.thenounproject.com/png/363639-200.png',
                }
            }
        ]
    },
    {
        title: '10 октября',
        data: [
            {
                time: '15:30',
                active: true,
                user: {
                    fullName: 'Абзал Суан',
                    diagnosis: 'удаление налета',
                    ava: 'https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png',
                }
            },
            {
                time: '17:30',
                user: {
                    fullName: 'Мартинас Оляка',
                    diagnosis: 'удаление зуба',
                    ava: 'https://static.thenounproject.com/png/363639-200.png',
                }
            }
        ]
    },
];

const HomeScreen = ({ navigation }) => {
    return (
        <Container>
            <SectionList
                sections={DATA}
                keyExtractor={(item, index) => index}
                renderItem={({ item }) => <Group navigate={navigation.navigate} {...item} />}
                renderSectionHeader={({ section: { title } }) => (
                    <GroupTitle>{title}</GroupTitle>
                )}
            />
            <AddItemButton>
                <Ionicons name="ios-add" size={36} color="white" />
            </AddItemButton>
        </Container>
    );
}
const AddItemButton = styled.TouchableOpacity`
  position: absolute;
  justify-content: center;
  align-items: center;
  border-radius: 50px;
  width: 64px;
  height: 64px;
  background: #2a86ff;
  right: 15px;
  bottom: 25px; 
`;
const Container = styled.View`
  flex: 1;
`;


export default HomeScreen;
